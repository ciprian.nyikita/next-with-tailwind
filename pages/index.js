
export default function Home() {
  return (
    <div className="container">
        <title>Movium</title>

      <main>
        <h1 className="title">
          Welcome to <a href="#">Movium</a>
        </h1>

        <p className="description">
         Let's get moving!
        </p>

      </main>

      <footer>
          Powered by Diana & Cipri
      </footer>
    </div>
  )
}
